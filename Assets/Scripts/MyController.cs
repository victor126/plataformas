﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Rigidbody2D))]

public class MyController : MonoBehaviour
{

    private Rigidbody2D rb2d = null;
    private float move = 0f;
    public float maxS = 11f;
    private bool jump;
    public float jf = 50.0f;
    public GameObject grafic;
    private float escalaAcrual;
    private bool attack = false;
    public GameObject personaje;

    
    public ScoreManager scripScore;
    [SerializeField] Animator animator;
    private bool isDead = false;

    public bool IsGrounded = false;
    // Use this for initialization
    void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        if (isDead) { return; }
        rb2d.AddForce(Vector2.right * move * maxS);
        //rb2d.velocity = new Vector2(move * maxS, rb2d.velocity.y);
        animator.SetFloat("run", Math.Abs(rb2d.velocity.x));
    }

    private void Update()
    {
		if (isDead) { return; }
        jump = Input.GetButtonDown("Jump");
        move = Input.GetAxis("Horizontal");
        attack = Input.GetButton("Fire1");

        if (jump && IsGrounded)
        {
            move = 0f;
            animator.SetBool("jump", jump);
            jump = false;
            rb2d.AddForce(Vector2.up * jf, ForceMode2D.Impulse);

            FindObjectOfType<AudioManager>().Play("salto");
        }

        escalaAcrual = grafic.transform.localScale.x;

        if (move > 0 && escalaAcrual < 0)
        {
            grafic.transform.localScale = new Vector3(1, 1, 1);
        }

        if (move < 0  && escalaAcrual > 0)
        {
            grafic.transform.localScale = new Vector3( -1, 1, 1);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        IsGrounded = true;
        animator.SetBool("jump", false);
        if (other.tag == "limite")
        {
            Debug.Log("Limite");
            isDead = true;
            StartCoroutine(Muerto());
            personaje.SetActive(false);
            FindObjectOfType<AudioManager>().Play("muerte");
        }
        else if(other.tag == "moneda")
        {
            Debug.Log("+ 1 Score");
            scripScore.AddPoints(5);
            Destroy(other.gameObject);
            FindObjectOfType<AudioManager>().Play("moneda");
        }

        if (other.tag == "Mortal")
        {
            animator.SetTrigger("muerte");
            isDead = true;
            StartCoroutine(Muerto());
            FindObjectOfType<AudioManager>().Play("muerte");
        }

        if (other.tag == "final")
        {
            SceneManager.LoadScene("Game Over");
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        IsGrounded = false;
    }


	private void OnCollisionEnter2D(Collision2D collision)
    {
       
        if (collision.gameObject.tag == "Mortal" )
        {
            Debug.Log("Muerte");
            animator.SetTrigger("muerte");
            isDead = true;
            StartCoroutine(Muerto());
            FindObjectOfType<AudioManager>().Play("muerte");
        }
        else if (collision.gameObject.tag == "PowerUp")
        {
            Debug.Log("Has pillado un powerup");
            maxS *= 4;
            Destroy(collision.gameObject);
        }
    }

    IEnumerator Muerto()
    {
        isDead = true;
        yield return new WaitForSeconds(1.0f);
        SceneManager.LoadScene("Game Over");
    }
}

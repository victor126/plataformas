﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AliadeBullet : MonoBehaviour {

	public float moveSpeed = 40f;
	public Rigidbody2D rb;

	// Use this for initialization
	void Start () {
        rb.velocity = transform.right * moveSpeed;
		Destroy (gameObject, 30f);
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.tag == "Mortal") {
			Debug.Log ("Hit!");
			Destroy (gameObject);
		}
	}

}

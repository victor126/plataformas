﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class fondo : MonoBehaviour
{
    private float length, startpos;
    public GameObject cam;
    public float delay;

    // Update is called once per frame
    void Start()
    {
        startpos = transform.position.x;
        length = GetComponent<SpriteRenderer>().bounds.size.x;
    }
    void FixedUpdate()
    {
        float temp = (cam.transform.position.x * (1 - delay));
        float dist = (cam.transform.position.x * delay);

        transform.position = new Vector3(startpos + dist, transform.position.y, transform.position.z);

        if(temp > startpos + length) startpos += length;
        else if(temp < startpos - length) startpos -= length;
    }
}
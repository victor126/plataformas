﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CodeMonkey.Utils;
using System;
public class PlayerAimWeapon : MonoBehaviour
{
    public EventHandler<OnShootEventArgs> OnShoot;

    public class OnShootEventArgs : EventArgs
    {
        public Vector3 GunEnPointPosition;
        public Vector3 ShootPosition;
        public Vector3 shellPosition;

    }

    public Transform aimTransform;
    public Animator aimAnimator;
    public GameObject muzzle;
    public Transform aimGunEndPointPosition;
    public Transform aimShellPositionTransform;
    private void FixedUpdate()
    {
        HandleShooting();
    }

    private void Update()
    {
        HandleAiming();
    }

    private void HandleAiming()
    {
        Vector3 mousePosition = UtilsClass.GetMouseWorldPosition();

        Vector3 aimDirection = (mousePosition - transform.position).normalized;
        float angle = Mathf.Atan2(aimDirection.y, aimDirection.x) * Mathf.Rad2Deg;
        aimTransform.eulerAngles = new Vector3(0, 0, angle);
    }

    private void HandleShooting()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            Vector3 mousePosition = UtilsClass.GetMouseWorldPosition();
            Vector3 aimDirection = (mousePosition - transform.position).normalized;
            aimAnimator.SetTrigger("shoot");
            muzzle.SetActive(true);
            OnShoot?.Invoke(this, new OnShootEventArgs
            {
                ShootPosition = aimDirection,
            });
        }
        else
        {
            muzzle.SetActive(false);
        }
    }
}

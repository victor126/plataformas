﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreManager : MonoBehaviour
{
    public TextMeshProUGUI puntuacion;

    public int score;

    public void AddPoints(int value)
    {
        score += value;
        puntuacion.text = score.ToString();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brazo : MonoBehaviour
{
    Vector3 lookAt;
    Vector3 mivector;
    float AngleRad;
    float AngleDeg;
    public Transform scale;

    // Update is called once per frame
    void Update()
    {
        mivector = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.nearClipPlane);
        //mivector = new Vector3(scale.localScale.x * mivector.x, mivector.y, mivector.z);  
        lookAt = Camera.main.ScreenToWorldPoint(mivector);

        AngleRad = Mathf.Atan2(lookAt.y - this.transform.position.y, lookAt.x - this.transform.position.x);

        AngleDeg = (180 / Mathf.PI) * AngleRad;
        if(scale.localScale.x < 0)
        {
            AngleDeg += 180;
        }

        this.transform.rotation = Quaternion.Euler(0, 0, AngleDeg);
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CodeMonkey.Utils;
public class Testing : MonoBehaviour
{

    [SerializeField] private PlayerAimWeapon playerAimWeapon;
    public Transform firePoint;
    public GameObject bullet;
    public float fireRate = 0f;
    public Transform player;

    float TimeTofire = 0;

    private void Start()
    {
        playerAimWeapon.OnShoot += PlayerAimWeapon_OnShoot;
    }

    private void PlayerAimWeapon_OnShoot(object sender, PlayerAimWeapon.OnShootEventArgs e)
    {
        UtilsClass.ShakeCamera(.09f, .1f);
        if(fireRate == 0f)
        {
            Shoot();
        }
        else if(Time.time > TimeTofire)
        {
            TimeTofire = Time.time + 1 / fireRate;
            Shoot();
        }
    }

    void Shoot()
    {
        Vector3 mousePosition = UtilsClass.GetMouseWorldPosition();
        Instantiate(bullet, firePoint.position, firePoint.rotation);
        RaycastHit2D hit = Physics2D.Raycast(firePoint.position, mousePosition - firePoint.position);
    }
}
